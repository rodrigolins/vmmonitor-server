from apps.books.models import Book, Author, BookAdmin, Publisher
from django.contrib import admin

admin.site.register(Book, BookAdmin)
admin.site.register(Author)
admin.site.register(Publisher)
