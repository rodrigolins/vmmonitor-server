from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic import DetailView, ListView
from django.core.urlresolvers import reverse_lazy
from django.contrib import messages
from .models import Script


class ScriptList(ListView):
    model = Script
    template_name = 'script_list.html'
    context_object_name = 'object_list'

    def get_queryset(self):
        return Script.objects.all()


class ScriptDetail(DetailView):
    model = Script
    template_name = 'script_detail.html'


class ScriptCreate(CreateView):
    model = Script
    template_name = 'script_create.html'
    fields = ['name', 'file', 'servers', 'description']

    success_url = reverse_lazy('script-list')
    #def get_success_url(self):
    #    return reverse_lazy('script-list')

    def form_valid(self, form):
        print form
        form.save(commit=True)
        messages.success(self.request, 'File uploaded!')
        return super(ScriptCreate, self).form_valid(form)


class ScriptDelete(DeleteView):
    model = Script
    template_name = 'script_delete.html'
    success_url = reverse_lazy('script-list')


class ScriptUpdate(UpdateView):
    model = Script
    template_name = 'script_update.html'

    def get_context_data(self, **kwargs):
        context = super(ScriptUpdate, self).get_context_data(**kwargs)
        context['action'] = reverse_lazy('script-edit', kwargs={'pk': self.get_object().id})
        return context