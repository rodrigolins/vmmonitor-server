from tastypie.authorization import Authorization
from tastypie.resources import ModelResource
from .models import Script
from .models import ScriptRun
import subprocess


class ScriptResource(ModelResource):
    class Meta:
        queryset = Script.objects.all()
        resource_name = 'script'
        authorization = Authorization()


class ScriptRun(ModelResource):
    class Meta:
        queryset = ScriptRun.objects.all()
        resource_name = 'run'
        list_allowed_methods = ['get', 'post']
        authorization = Authorization()

    #def dehydrate(self, bundle):
    #    print 'dehydrate'
    #    data = bundle.data['status'] = 'laalala'
    #    return data

    def hydrate(self, bundle):
        script_id = bundle.data['script_id']
        run_script = Script.objects.get(pk=script_id)
        print run_script.file.name
        server_id = bundle.data['server_id']
        status = bundle.data['status']
        #subprocess_output = subprocess.check_output('/home/rodrigo/sourcecode/vmmonitor-server/scripts/' + run_script.file.name)
        print run_script.file.name
        subprocess_output = subprocess.check_output('C:/Users/rodrigo/PycharmProjects/VMMonitor/userfiles/' + run_script.file.name)
        print subprocess_output
        bundle.data['status'] = subprocess_output
        print 'Script: %s | Server: %s | Status: %s' % (script_id, server_id, status)
        return bundle

    #def dehydrate(self, bundle):
    #    print 'aqui2'
    #    bundle.data['result'] = "Whatever you want"
    #    return bundle