from django.db import models
from django.core.urlresolvers import reverse
from django.forms import ModelForm

from apps.servers.models import Server


class Script(models.Model):
    name = models.CharField(max_length=100)
    file = models.FileField(upload_to='.')
    description = models.TextField()
    servers = models.ManyToManyField(Server)

    def get_absolute_url(self):
        return reverse('script-detail', kwargs={'pk': self.pk})

    def __str__(self):
        return self.name


class ScriptFrom(ModelForm):
    class Meta:
        model = Script


class ScriptRun(models.Model):
    script_id = models.CharField(max_length=10)
    server_id = models.CharField(max_length=10)
    status = models.TextField()


class ScriptRunForm(ModelForm):
    class Meta:
        model = ScriptRun