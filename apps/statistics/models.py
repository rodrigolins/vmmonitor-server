from django.db import models


class Statistic(models.Model):
    server_id = models.IntegerField()
    cpu = models.IntegerField()
    memory = models.CharField(max_length=14)
    network = models.CharField(max_length=30)
    disk = models.CharField(max_length=100)
    processes = models.IntegerField()

    def __str__(self):
        return self.disk
