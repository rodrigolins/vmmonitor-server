from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.contrib.auth.models import User
from django import forms
from apps.users.models import ServerUser


class ServerUserCreationForm(UserCreationForm):
    description = forms.CharField(max_length=100)

    class Meta:
        model = User
        fields = ['username', 'password1', 'password2', 'email', 'description']

    def save(self, commit=True):
        if not commit:
            raise NotImplementedError("Can't create SeverUser and UserProfile without database save")
        user = super(ServerUserCreationForm, self).save(commit=True)
        server_user = ServerUser(user=user, description=self.cleaned_data['description'])
        server_user.save()
        return user, server_user


class ServerUserChangeForm(UserChangeForm):
    description = forms.CharField(max_length=100)

    class Meta:
        model = User
        fields = ['username', 'email', 'description']
        exclude = ['password']

    def __init__(self, *args, **kwargs):
        super(ServerUserChangeForm.user, self).__init__(*args, **kwargs)
        if self.instance and self.instance.pk:
            self.fields['username'] = self.instance.user.username
            self.fields['email'] = self.instance.user.email
            self.fields['description'] = self.instance.description
