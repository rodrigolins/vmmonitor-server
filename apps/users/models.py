from django.db import models
from django.contrib.auth.models import User
from django.forms import ModelForm
from django.core.urlresolvers import reverse


class ServerUser(models.Model):
    user = models.OneToOneField(User)
    description = models.CharField(max_length=100)

    def get_absolute_url(self):
        return reverse('user-detail', kwargs={'pk': self.pk})

    def __str__(self):
        return self.user.username


class ServerUserForm(ModelForm):
    class Meta:
        model = ServerUser