from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic import DetailView, ListView
from apps.users.models import ServerUser
from django.core.urlresolvers import reverse_lazy
from apps.users.forms import ServerUserCreationForm, ServerUserChangeForm


class UserList(ListView):
    model = ServerUser
    template_name = 'user_list.html'
    context_object_name = 'object_list'

    def get_queryset(self):
        return ServerUser.objects.all()


class UserDetail(DetailView):
    model = ServerUser
    template_name = 'user_detail.html'


class UserCreate(CreateView):
    model = ServerUser
    template_name = 'user_create.html'
    form_class = ServerUserCreationForm

    def get_success_url(self):
        return reverse_lazy('user-list')


class UserDelete(DeleteView):
    model = ServerUser
    template_name = 'user_delete.html'
    success_url = reverse_lazy('user-list')


class UserUpdate(UpdateView):
    model = ServerUser
    template_name = 'user_update.html'
    form_class = ServerUserChangeForm

    def get_context_data(self, **kwargs):
        context = super(UserUpdate, self).get_context_data(**kwargs)
        context['action'] = reverse_lazy('user-edit', kwargs={'pk': self.get_object().id})
        return context

    def get_object(self, queryset=None):
        return self.request.user