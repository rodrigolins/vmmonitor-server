__author__ = 'rodrigo'
from django.shortcuts import get_object_or_404
from django.utils.html import strip_tags
from django_socketio import events

@events.on_message(channel="^connect")
def message(request, socket, context, message):
    print 'events.on_message'
    socket.send({"action": "works!"})

@events.on_finish(channel="^connect")
def finish(request, socket, context):
    print 'events.on_finish'

