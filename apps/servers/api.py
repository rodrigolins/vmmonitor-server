from tastypie.authorization import Authorization
from tastypie.resources import ModelResource
from tastypie import fields
from .models import Server
from apps.scripts.models import Script


class ServerResource(ModelResource):
    scripts = fields.ToManyField('apps.servers.api.ScriptResource', 'script_set', related_name='server')

    class Meta:
        queryset = Server.objects.all()
        resource_name = 'server'
        authorization = Authorization()


class ScriptResource(ModelResource):
    script = fields.ToOneField(ServerResource, 'server')

    class Meta:
        queryset = Script.objects.all()
        resource_name = 'script'
        authorization = Authorization()