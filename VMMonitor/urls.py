from django.conf.urls import patterns, include, url

from django.contrib import admin
from monitor.views import Index
from apps.servers.views import ServerCreate, ServerUpdate, ServerDelete, ServerList, ServerDetail
from apps.scripts.views import ScriptList, ScriptDetail, ScriptCreate, ScriptUpdate, ScriptDelete
from apps.users.views import UserList, UserDetail, UserCreate, UserUpdate, UserDelete
from apps.alerts.views import AlertList, AlertDetail, AlertCreate, AlertUpdate, AlertDelete
from apps.scripts.api import ScriptResource
from apps.scripts.api import ScriptRun
from apps.servers.api import ServerResource
from apps.users.api import ServerUserResources
from apps.statistics.api import StatisticResource
from apps.statistics.api import SpecificStatisticResource

admin.autodiscover()

script_resource = ScriptResource()
run_resource = ScriptRun()
server_resource = ServerResource()
user_resource = ServerUserResources()
statistic_resource = StatisticResource()
specific_statistic_resource = SpecificStatisticResource()

urlpatterns = patterns('',
                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^search/$', 'apps.books.views.search'),
                       url(r'^$', Index.as_view(), name='index'),

                       url(r'^server-detail/(?P<pk>\d+)/$', ServerDetail.as_view(), name='server-detail'),
                       url(r'^server-create/$', ServerCreate.as_view(), name='server-create'),
                       url(r'^server-update/(?P<pk>\d+)/$', ServerUpdate.as_view(), name='server-update'),
                       url(r'^server-delete/(?P<pk>\d+)/$', ServerDelete.as_view(), name='server-delete'),
                       url(r'^server-list/$', ServerList.as_view(), name='server-list'),

                       url(r'^alert-detail/(?P<pk>\d+)/$', AlertDetail.as_view(), name='alert-detail'),
                       url(r'^alert-create/$', AlertCreate.as_view(), name='alert-create'),
                       url(r'^alert-update/(?P<pk>\d+)/$', AlertUpdate.as_view(), name='alert-update'),
                       url(r'^alert-delete/(?P<pk>\d+)/$', AlertDelete.as_view(), name='alert-delete'),
                       url(r'^alert-list/$', AlertList.as_view(), name='alert-list'),

                       url(r'^script-detail/(?P<pk>\d+)/$', ScriptDetail.as_view(), name='script-detail'),
                       url(r'^script-create/$', ScriptCreate.as_view(), name='script-create'),
                       url(r'^script-update/(?P<pk>\d+)/$', ScriptUpdate.as_view(), name='script-update'),
                       url(r'^script-delete/(?P<pk>\d+)/$', ScriptDelete.as_view(), name='script-delete'),
                       url(r'^script-list/$', ScriptList.as_view(), name='script-list'),

                       url(r'^user-list/$', UserList.as_view(), name='user-list'),
                       url(r'^user-detail/(?P<pk>\d+)/$', UserDetail.as_view(), name='user-detail'),
                       url(r'^user-create/$', UserCreate.as_view(), name='user-create'),
                       url(r'^user-update/(?P<pk>\d+)/$', UserUpdate.as_view(), name='user-update'),
                       url(r'^user-delete/(?P<pk>\d+)/$', UserDelete.as_view(), name='user-delete'),

                       url(r'api/', include(script_resource.urls)),
                       url(r'api/', include(run_resource.urls)),
                       url(r'api/', include(server_resource.urls)),
                       url(r'api/', include(user_resource.urls)),
                       url(r'api/', include(statistic_resource.urls)),
                       url(r'api/', include(specific_statistic_resource.urls)),

                       url(r'', include('gcm.urls')),
                       url("", include('django_socketio.urls')),
                       url(r'^socket/$', 'apps.sockets.views.connect', name='socket'),
)
