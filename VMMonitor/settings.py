import os.path
"""
Django settings for VMMonitor project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
import djcelery
djcelery.setup_loader()
CELERY_ALWAYS_EAGER = True

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

PROJECT_ROOT = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/var/www/example.com/media/"
# If PROJECT_ROOT is '/home/myuser/webapps/uploadering/' then
# MEDIA_ROOT is '/home/myuser/webapps/uploadering/userfiles/'
#
MEDIA_ROOT = os.path.join(PROJECT_ROOT, 'userfiles')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://example.com/media/", "http://media.example.com/"
#
MEDIA_URL = '/files/'  # Note they don't have to be identical names

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'mkr3skhv$0j0-ekb#8dx#$gtd!@$o$$m7h-vlc8rf7&n7_b-+v'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []

#USING: 141.76.177.0/24 - University
GCM_APIKEY = "AIzaSyAI4lUvF59NoBAmn3x8gdSlT5B-lnjaYZs"
#USING: 192.168.100.0/24 - Home
#GCM_APIKEY = "AIzaSyCbRZ8WkIp5pfneotToa8yP86nLDo3883c"

# 78.53.84.96
#GCM_APIKEY = "AIzaSyB3Ua2-39rtky2tZs2uXsqLKQNSBgMLJvU"


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'monitor',
    'apps.users',
    'apps.scripts',
    'apps.servers',
    'apps.alerts',
    'apps.sockets',
    'apps.statistics',
    'django.contrib.admin',
    'apps.books',
    'django_socketio',
    'gcm',
    'djcelery',
)

CELERYBEAT_SCHEDULER = "djcelery.schedulers.DatabaseScheduler"


MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'VMMonitor.urls'

WSGI_APPLICATION = 'VMMonitor.wsgi.application'

TEMPLATE_DIRS = (
    os.path.join(os.path.dirname(__file__), '../VMMonitor/../VMMonitor/templates').replace('\\', '/'),
)

# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'VMMonitor',
        'USER': 'root',
        'PASSWORD': 'root',
        'HOST': 'localhost',
        'PORT': '3306',
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'

#AUTH_PROFILE_MODULE = 'apps.users.models.ServerUser'